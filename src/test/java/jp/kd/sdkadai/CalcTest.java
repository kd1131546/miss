/**
 * ソフトウェア開発実験 期末課題(その2)
 * 
 * 以下の各問に対応する実装をmainディレクトリ側で作成して、テストが通るように実装していくこと
 * 基本的には上から下の順ですすめるようになっている
 * 設問1以外はコメントで隠しているため、次へ進むときは適宜コメントを外して対応していくこと
 * 各設問末尾のA,B,Cは難易度である(Cが易しい→Aが難しい)
 * 
 * 提出期限は9月25日(木)24時(26日0時)までとする。確認等は未提出者に対する確認などは一切行わないため、余裕を持って行うこと
 * なお、うまく行っているかどうかは各自のJenkinsでチェックできるはずなので、きちんと確認して行うこと。
 */

package jp.kd.sdkadai;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;

public class CalcTest {


	// ↓設問4の際にコメントを解除してください
	//private MyCalc m = null;

	// 設問4にて使用
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	// 設問4にて使用
	
	// 設問1: クラス jp.kd.sdkadai.MyCalc を作成し、以下のテストが通るようにせよ、src/main/java側で作ること(B)
	
	/** クラス実装の存在チェック */
	@Test
	public void testNew() {
		try {
			Class.forName("jp.kd.sdkadai.MyCalc");
		} catch (ClassNotFoundException e) {
			fail("MyCalcクラスが未実装です");
		}
	}

	// ここから下の問題は、やるところのみ該当するコメントを解除して進めていくこと
	// 実装を満たすための最小限のコードで実装すること(余計なことはしない)

/*
	// 設問2: MyCalcクラスを実装し、下記のコードが通るようにせよ(B) 
	@Test
	public void testCalcInit() {
		MyCalc m = new MyCalc();
		assertThat(m.getNum(), is(0)); // 返す初期値はゼロ
	}
*/


/*
	// 設問3: 内部フィールド num(Integer型)をprivateで用意し、getter/setterメソッドを定義せよ(B)
	@Test
	public void testGetSet() {
		MyCalc m = new MyCalc();
		m.setNum(5);
		assertThat(m.getNum(), is(5));
		
	}
*/
	
 
	 //設問4: 設問2,3では各テスト内でmにインスタンスを入れているが、いちいち行うのは面倒である。
	 //       そこで、本テストクラスで用意しているMyCalcクラスの変数mを利用し、テストの前にnewするようにコードを追加せよ
	 //       本テストコードの冒頭にあるMyCalc mのコメントを外し、直後指示のある部分のエリアにテストが継続できるようコードを追記せよ
	 //       (なお、設問2,3はいじる必要はない、メソッド内で同一名を宣言した場合、内側のものが優先されるためである)
	 //       (C)
	 

	// ----- ここ以降のテストコードはコメントを外すのみでコードそのものはいじらないこと(いじったことがわかった場合は無効とする)
/*
	// 設問5: 以下のテストを満たすコードを実装せよ(B) 
	@Test
	public void testCalcEx1() {
		m.setNum(5);
		assertThat(m.ex1(1), is(5));
		assertThat(m.ex1(2), is(10));
		assertThat(m.ex1(3), is(15));		
	}
*/

/*
	// 設問6: 以下のテストを満たすコードを実装せよ、もちろん設問5を壊してはいけない(B)
	@Test
	public void testCalcEx2() {
		m.setNum(5);
		assertThat(m.ex1(-1), is(5));
		assertThat(m.ex1(-2), is(10));
		assertThat(m.ex1(-3), is(15));		
	}
*/

/*
	// 設問7: 以下のテストを満たすコード(素数)を実装せよ、ただし実装の中にjava.lang.Mathパッケージのメソッドを使ってはいけない(B)
	// commons利用は禁止 
	@Test
	public void testCalcEx3() {
		m.setNum(1);
		assertThat(m.ex2(), is(false));
		m.setNum(2);
		assertThat(m.ex2(), is(true));
		m.setNum(9);
		assertThat(m.ex2(), is(false));
		m.setNum(-5);
		assertThat(m.ex2(), is(false)); // 1未満の数はすべてfalseという設定にする		
	}
*/
	
/*
	// 設問8: 以下のテストを満たすコードを作成せよ、評価している型に注意すること(A) 
	@Test
	public void testEx4() {
		m.setNum(1);
		assertThat(m.ex3(m.getNum()), is("1"));
		m.setNum(2);
		assertThat(m.ex3(m.getNum()), is("4"));
		m.setNum(8);
		assertThat(m.ex3(m.getNum()), is("16777216"));
		m.setNum(16);
		assertThat(m.ex3(m.getNum()), is("18446744073709551616"));
		m.setNum(32);
		assertThat(m.ex3(m.getNum()), is("1461501637330902918203684832716283019655932542976"));
		// hint: http://docs.oracle.com/javase/jp/7/api/java/math/BigInteger.html		
	}
*/
	
// 以上で終了です、忘れずにpushしておくようにしてください
}
